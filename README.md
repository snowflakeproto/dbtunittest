# Unit Testing on DBT models using Static test dataset.

## Overview
This prototype project is used for demonstrating unit testing in DBT using static test dataset.

## Introduction
Testing on dbt built models is one of the core functionality, that sometimes are overlooked. If you are wondering why you need to test your model, i would recommend to read this [Blog: Automated testing in the modern data warehouse](https://medium.com/@josh.temple/automated-testing-in-the-modern-data-warehouse-d5a251a866af).

Out of the box; dbt provides two different mechanisms for data validation schema tests and custom data tests. These however are basic
in my opinion. They do not clearly articulate how the data should be transformed or the expected output. I also find it redundant in
writing expressions both in the model as well as a [Testing expression](https://dbt.readme.io/docs/testing#testing-expressions). A coding
error could happen in the model or in the expression definition itself. Another point of view is that, if the model involved complex window based operations like (first_value, last_value etc..) these cannot be easily expressed in these test dataset.

Based on my experience, it was always easy that I as a developer sat down with a business analyst ,pull up an excel sheet and 
fill input values (from the input table) and expected output values (in the transformed table). This gave us both an understanding and confirmation.

![](docs/images/input_output_tranformation_sheet.jpg)

While doing this exercise it was also easy for me to understand complex operations that the analyst wanted me to perform. Be it 
a date difference calculation or window operations or applying average across a specific groups.

If you reconnect with your own experience, expressing these in the testing expression is simply not possible.

A discussion on the [dbt discource: Testing with fixed data set](https://discourse.getdbt.com/t/testing-with-fixed-data-set/564/5) gave a direction on how we can perform
actual data testing using DBT gave some insights. I highly recommend reading the discource link
to get an initial understanding of the discussion and how it was solved. This prototype code demonstrated 
here is an implementation and walkthrough of how this can be done, based of the discussion. 

**NOTE**: The model/transformation involved are meant for demonstration purposes, the business logic behind why the calculation is done and how it is done, should not 
be of concern. The underlying intent is to demonstrate how to perform the testing on a test data set.

### Audience
I am expecting the audience to have experience in DBT model building and understanding the functionality on 
- [seed](https://docs.getdbt.com/docs/running-a-dbt-project/command-line-interface/seed/)
- [deps](https://docs.getdbt.com/docs/running-a-dbt-project/command-line-interface/deps)
- [run](https://docs.getdbt.com/docs/running-a-dbt-project/command-line-interface/run)
- [Testing](https://docs.getdbt.com/docs/building-a-dbt-project/testing-and-documentation/testing/)

## Scenario
To keep things short and simple, I am using the "LINEITEM" table from Snowflake Sample Dataset (TPCH_SF1). The model behaviour is as follows

- Copy a sample of the 'snowflake_sample_data.tpcH_sf1.lineitem' records into a 'order_line' table
- Derive calculated field 'line_price' and 'discounted_line_price'
- For each order aggregate  no of line items ,line_price ,discounted_line_price
- Materialize the result into 'ORDER_LINEPRICE_SUM' table.

The screenshot below demonstrates what is the input and the expected result after
completion of transformation.

![](docs/images/input_output_tranformation_sheet.jpg)

The following diagram depicts the datapipeline based of dbt docs:

![](docs/images/model_transformation.jpg)

### Execution 
The set of commands to run the test is as follows:
```shell
# Download dbt dependencies
dbt deps

# Seed the test datasets
dbt seed

# Execute the model
dbt run -m +ORDER_LINEPRICE_SUM --target=test --vars "{'ORDER_LINE_K': 'TEST_DB.PUBLIC.ORDER_LINE_INPUT_IDL'}"

# run the test
dbt test
```
I will be explaining on the details in the later section, for now look at the below screenshots which demonstrates the sample output of execution run

#### Successful execution

The datapreview of 'ORDER_LINEPRICE_SUM':
![](docs/images/successful_execution.jpg)

The command line output of dbt run:
![](docs/images/successful_execution_dbt.jpg)

#### Failed execution
To demonstrate a failure scenario, i am replacing
``` sql
  select 
    l_orderkey 
    ,count(l_linenumber) as no_of_lines
    ,sum(line_price) as total_line_price
    ,sum(discounted_line_price) as total_discounted_line_price
  from line
  group by l_orderkey
  order by l_orderkey desc
```
with
``` sql
  select 
    l_orderkey 
    ,SUM(l_linenumber) as no_of_lines --purposefully done to demonstrate wrong calculation
    ,sum(line_price) as total_line_price
    ,sum(discounted_line_price) as total_discounted_line_price
  from line
  group by l_orderkey
  order by l_orderkey desc
```

The datapreview of 'ORDER_LINEPRICE_SUM':
![](docs/images/failure_execution.jpg)

The output of the dbt run is as below: 

![](docs/images/failure_execution_dbt.jpg)

The failure is due to wrong calculation done on the no of lines.

### Implementation 

The test data are stored in the "test-data" folder, which is configured as a "data-path" in the dbt_project.yml

I follow a convention that <<source_table>>_INPUT to indicate that this seeded data will be used as an input. And the <<target_table>>_EXPECTED to indicate that this is expected result once the model transformation is completed.

The testing is defined in the [TEST_DB_SCHEMA.yml](test-data/TEST_DB_SCHEMA.yml) file. The definition is as below:
```yaml
seeds:
  - name: ORDER_LINEPRICE_SUM_EXPECTED 
    tests:
      - dbt_utils.equal_rowcount:
          compare_model:  ref('ORDER_LINEPRICE_SUM')
      - dbt_utils.equality:
          compare_model: ref('ORDER_LINEPRICE_SUM')
          compare_columns:
            - L_ORDERKEY
            - NO_OF_LINES
```

You would have to wrap your thoughts in a different way, such that you are testing against
the expected seed table. Defining the test against the target model (ORDER_LINEPRICE_SUM) does
not handle well in all situations. Whereas defining test against a seeded data is easier.

If you have different variations of input, since the underlying table changes from initial data load
to ongoing delta loads, you would also end up with different versions of the expected resultant records
also. Hence defining the test against this variation with the same underlying table is possible. 

You can also test against a specific seed like below
```shell
  dbt test --schema -m ORDER_LINEPRICE_SUM_EXPECTED
```

#### Pointing to input seed table
If you had followed the initial [dbt discource: Testing with fixed data set](https://discourse.getdbt.com/t/testing-with-fixed-data-set/564/5); it was discussed that a custom implementation of 'ref' macro might
needed to be developed. This is how I have it done.

#### Macro : tbref
I have the macro 'tbref' defined in [macros/utility_functions.sql](macros/utility_functions.sql. This
function takes 2 parameter:
- model_name_key => a key to look for in the var command line argument
- model_name => the default model, incase the var is not supplied

a sample call of this macro is as follows [ex: ORDER_LINEPRICE_SUM.sql](models/TEST_DB/PUBLIC/ORDER_LINEPRICE_SUM.sql)

```
  from {{ tbref('ORDER_LINE_K' ,'ORDER_LINE') }}
```

By default target, the model 'ORDER_LINE' will be taken into consideration. When we are performing
a 'test target' and if the var argument is supplied with the 'ORDER_LINE_K' then the model present
in the var argument will be considered. The 'taget=test' is done to indicate testing and as a preventive measure.

#### Command line 
Thus the below command line (mentioned earlier), indicates that dbt run is against the test target
and the ORDER_LINE_K points to table 'TEST_DB.PUBLIC.ORDER_LINE_INPUT_IDL'; which will be used
as the input to the 'ORDER_LINEPRICE_SUM' model transformation

```shell
# Execute the model
dbt run -m +ORDER_LINEPRICE_SUM --target=test --vars "{'ORDER_LINE_K': 'TEST_DB.PUBLIC.ORDER_LINE_INPUT_IDL'}"
```
If you want to run under default target, the invocation will be as follows:
```shell
# Execute the model
dbt run -m +ORDER_LINEPRICE_SUM
```

## Final thoughts
What was demonstrated here is a simple and effective way to do the unit data testing. The test data preparation 
would definetively take your development cycle times. But it protects you from future accidental mistakes as 
you develop future model or enhance existing model.

I hope this short prototype helps you at the very least as an initial short to get started.






