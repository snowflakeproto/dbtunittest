{{ config(materialized='table') }}

-- This 

with line as (
  select 
    l_orderkey 
    ,l_linenumber
    ,l_quantity * l_extendedprice as line_price
    ,line_price + (line_price * l_discount) as discounted_line_price
  from {{ tbref('ORDER_LINE_K' ,'ORDER_LINE') }}
)
select 
  l_orderkey 
  ,count(l_linenumber) as no_of_lines
  ,sum(line_price) as total_line_price
  ,sum(discounted_line_price) as total_discounted_line_price
from line
group by l_orderkey
order by l_orderkey desc
