{{ config(materialized='table') }}

-- Copies a sample set of records from the snowflake sample dataset
-- and materialize into a table. By doing this we would able to reference
-- the table in the transformation model. In an actual implementation, this
-- would be pointing to a table that was ingested from the source.
-- 

select *
  from {{ source('SAMPLE' ,'LINEITEM') }}
  limit 10000