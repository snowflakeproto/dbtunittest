{% macro tbref(model_name_key ,model_name) %}
  {#
    This is a custom macro that override the default 'ref' macro usage. Its 
    been developed to address the following needs:
      - Facilitate data testing with static datasets : https://discourse.getdbt.com/t/testing-with-fixed-data-set/564/5
  #}
  
 {% if target.name is not defined %}
    {{ return(model_name) }}
  {% elif var is not defined %}
    {{ return(model_name) }}
  {% endif %}

  {% if target.name == 'test' and var(model_name_key) %}
    {{ log("Loading from reference : " ~ var(model_name_key),True) }}
    {{ return(var(model_name_key)) }}
  {% endif %}
  
  {{ log("Loading default model : " ~ model_name,True) }}
  {{ return(model_name) }}
{% endmacro %}
